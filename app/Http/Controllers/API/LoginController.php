<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Auth;
class LoginController extends Controller
{
    public function login(Request $request) {
    	$credentials = ['email' => $request->get('email'), 'password'=>$request->get('password')];
        // dd($credentials);
    	if (! $token = Auth::guard('api')->attempt($credentials)) {
           return response()->json(['success'=>false,'error'=>true,'msg' =>'Please enter valid credentials','code'=>401]);
        }

        $token = $this->respondWithToken($token);

        $data = Auth::user();

        return response()->json(['error'=>false,'data'=>$data,'success'=>true,'token' =>$token['access_token'],'msg'=>'You have logged in successfully']);
    }

    protected function respondWithToken($token)
    {
        return [
            'access_token' => $token
        ];
    }
}
