<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Validator;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
	public function register(Request $request) {
		$data = $request->all();
	    $validator = Validator::make($data, [
	            'first_name' => 'required|string|max:60',
	            'last_name' => 'required|string|max:60',
	            'email' => 'required|email|max:100|unique:users,email',
	            'password' => 'required|min:6',
            	'c_password' => 'required|same:password'              
	     ]);

	    if ($validator->fails()) {
	        return response()->json(['msg'=>'Registration Unsuccessful','data'=>$validator->errors(),'error'=>true,'success'=>false,'code'=>401]); 
	    }

	    $user = User::create([
	    	'first_name'=>$data['first_name'],
	    	'last_name'=>$data['last_name'],
	    	'email'=>$data['email'],
	    	'password'=>Hash::make($data['password']),
	    ]);
	    return response()->json( ['msg'=>'Registration Successful','error'=>false,'success'=>true,'code'=>200]); 
    }
}
