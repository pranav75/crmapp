<?php

namespace App\Http\Controllers\API\Password;

use App\Http\Controllers\Controller;
use App\Transformers\Json;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Auth;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class ResetPasswordController extends Controller
{
    public function resetPassword(Request $request)
    {
            //$user=User::where('email')
    try{

        $data=$request->all();
        //dd($data);
        $user=User::where('user_forgot_password_token', $data['user_forgot_password_token'])->first();
       
        if($user!=null)
        {
            if($data['password']==$data['cpassword'])
            {
                $user->password= Hash::make($data['password']);
                $user->user_forgot_password_token=null;
                $user->save();
                return response()->json(['success' => true,'error'=>false,'msg'=>'user password updated']);
            }
            else{
                return response()->json(['success' => false,'error'=>true,'msg'=>'password does not match']);
            }
        }
        else{
                return response()->json(['success' => false,'error'=>true,'msg'=>'user not found']);
        }
      }
        catch (Exception $e) {
        $err= $e->getMessage();
        return response()->json(['data'=>$err,'error'=>true,'success'=>false]); 
      }

    }
}
