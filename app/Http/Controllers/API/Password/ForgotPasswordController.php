<?php

namespace App\Http\Controllers\API\Password;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;
use App\Mail\forgotPassword;
use App\Models\User;
use Auth;
use App\Transformers\Json;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{

    use SendsPasswordResetEmails;
    public function getResetToken(Request $request) {
     
        $this->validate($request, ['email' => 'required|email']);
        
        if ($request->wantsJson()) {

            $user = User::where('email', $request->email)->first();
            
            if (!$user) {
                return response()->json(['msg'=>'user not found','success'=>false,'error'=>true,'code'=>401]);
            }
            $token = $this->broker()->createToken($user);
            $user->user_forgot_password_token=$token;
            $user->save();
            
            Mail::to($request->email)->send(new forgotPassword($user));

            return response()->json(['msg'=>'Please click on the link sent to your mail id','success'=>true,'error'=>false,'code'=>200]);
        }
    }
}
